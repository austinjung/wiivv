app.controller('ItemController', ['$http', '$q', '$filter', function ($http, $q, $filter) {
    var ctrl = this;

    this.support_currency = ['USD', 'CAD', 'GBP', 'EUR'];
    this.usd = true;
    this.cad = false;
    this.gbp = false;
    this.eur = false;

    this.changeCurrency = function changeCurrency(currency) {
       if (currency == 'usd') {
           this.cad = false;
           this.gbp = false;
           this.eur = false;
       }
       else if (currency == 'cad') {
           this.usd = false;
           this.gbp = false;
           this.eur = false;
       }
       else if (currency == 'gbp') {
           this.cad = false;
           this.usd = false;
           this.eur = false;
       }
       else if (currency == 'eur') {
           this.cad = false;
           this.gbp = false;
           this.usd = false;
       }
    } ;

    this.displayed = [];
    ctrl.isLoading = true;

    this.callServer = function callServer(tableState) {

        ctrl.isLoading = true;

    	var api_url = '/api/items/';
        var pagination = tableState.pagination;

        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.
        var ordering = tableState.sort.predicate ? tableState.sort.predicate : '-rating';
        if (ordering.includes('price')) {
            ordering = 'price';
        }
        if (tableState.sort.reverse) {
            ordering = '-' + ordering;
        }
        var params = {
            page: start / number + 1,
            ordering: ordering
        };
        if (tableState.search.predicateObject) {
            params.search = tableState.search.predicateObject.title;
        }

   		var deferred = $q.defer();

        deferred.promise = $http.get(api_url, {params: params, timeout: deferred.promise})
        .success(function(data) {
            ctrl.displayed = data.results;
            if ((data.count % data.page_size) > 0) {
                tableState.pagination.numberOfPages = Math.floor(data.count / data.page_size) + 1;
            } else {
                tableState.pagination.numberOfPages = Math.floor(data.count / data.page_size);
            }
            ctrl.isLoading = false;
        });

    };

}]);
