var app = angular.module('MyApp',['smart-table'])
.config(['$httpProvider',
    function(provider) {
        provider.defaults.xsrfCookieName = 'csrftoken';
        provider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }
]);
