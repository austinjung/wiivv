def get_black_rectangle(two_dim_arr):
    """
    :param two_dim_arr:
    :return: list of black rectangle left-top and right-bottom coordinator with zero-based index
    """

    black_rectangle_list = []  # return value: list of black rectangle left-top and right-bottom coordinator with zero-based index

    rows = len(two_dim_arr)  # rows = 3
    cols = len(two_dim_arr[0])  # cols = 4

    if rows == 0 or cols == 0:  # empty two dimensional array, return empty list of black rectangle
        return black_rectangle_list

    visited = [[False for c in range(cols)] for row in range(rows)]  # For memorize visited cell
    for row in range(rows):  # from top row
        for col in range(cols):  # from left column
            if two_dim_arr[row][col] == 0 and visited[row][col] is False:  # if current cell is start of a new black rectangle
                left_top = (row, col)
                right_bottom = left_top  # right_bottom initial value is same as left_top
                right_bottom = mark_field(two_dim_arr, row, col, rows, cols, visited, right_bottom)
                black_rectangle_list.append((left_top, right_bottom))

    return black_rectangle_list


def mark_field(arr, row, col, rows, cols, visited, right_bottom):
    if visited[row][col]:
        return right_bottom
    visited[row][col] = True
    if arr[row][col] == 1:
        return right_bottom
    if row > right_bottom[0]:
        right_bottom = (row, right_bottom[1])
    if col > right_bottom[1]:
        right_bottom = (right_bottom[0], col)
    ## This is a stating point:upper left of new field
    ## Start marking same field as visited
    if row > 0 and arr[row-1][col] == 0:
        right_bottom = mark_field(arr, row-1, col, rows, cols, visited, right_bottom)
    if row + 1 < rows and arr[row+1][col] == 0:
        right_bottom = mark_field(arr, row+1, col, rows, cols, visited, right_bottom)
    if col > 0 and arr[row][col-1] == 0:
        right_bottom = mark_field(arr, row, col-1, rows, cols, visited, right_bottom)
    if col + 1 < cols and arr[row][col+1] == 0:
        right_bottom = mark_field(arr, row, col+1, rows, cols, visited, right_bottom)
    return right_bottom


if __name__ == '__main__':
    two_dimension =  [
        [1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1],
        [1, 0, 1, 1, 1, 1, 1],
        [1, 0, 1, 0, 0, 1, 1],
        [1, 1, 1, 0, 0, 1, 1],
        [1, 1, 1, 1, 1, 1, 1],
    ]
    print get_black_rectangle(two_dimension)
