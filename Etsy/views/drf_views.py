from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import filters
from Etsy.utils import *
from Etsy.serializers import *
import requests
import json


class APIViewSet(viewsets.ViewSet):
    """
    API endpoint urls
    """
    def list(self, request, *args, **kwargs):
        return Response(ShowUrls().get_endpoints(request))


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    http_method_names = ['head', 'options', 'get']
    filter_backends = (filters.OrderingFilter, filters.SearchFilter)
    search_fields = ('title', )
    ordering_fields = ('rating', 'price')
    ordering = ('-rating',)  # Default ordering is most popular
    currency_table = {}

    def list(self, request, *args, **kwargs):
        page_url = 'http://www.apilayer.net/api/live?access_key=00a02e7e0332ae8a9de4eb0269433071&format=1'
        response = requests.get(page_url)
        if response.status_code != 200:
            raise requests.HTTPError('HTTP error: %d' % response.status_code)
        json_content = json.loads(response.content)['quotes']
        self.currency_table['USDCAD'] = json_content['USDCAD']
        self.currency_table['USDGBP'] = json_content['USDGBP']
        self.currency_table['USDEUR'] = json_content['USDEUR']
        return super(ItemViewSet, self).list(request, *args, **kwargs)
