from django.core.management.base import BaseCommand
from Etsy.models import Item
import random

class Command(BaseCommand):
    args = ''
    help = 'Create items'
    category = ['Mug', 'Watch']
    thumbnail = {
        'Mug': '/static/images/mug.jpg',
        'Watch': '/static/images/watch.jpg'
    }
    description = {
        'Mug':
"""Leak proof lid (when closed) for on-the-go activities.
Drinks stay hot up to 7 hours and cold up to 18 with THERMALOCK Vacuum Insulation.
Easy one-handed drinking from this travel coffee mug.
Fits most car cup holders.
Tumbler features easy grip to grab and go.
""",
        'Watch':
"""Highest standard Swiss movement.
Stainless-steel bezel with 0.16 CTW genuine diamonds.
Decorative pave dial with three time zone capability; date, day, month, and military time functions.
Stainless-steel case and genuine leather band with buckle.
Water-resistant to 165-Feet (50 M).
"""
    }

    def handle(self, *args, **options):
        if Item.objects.count() > 0:
            self.stdout.write("We already have items in DB.")
            return
        for i in range(1, 129):
            category = self.category[random.randint(0, 1)]
            item = Item()
            item.title = "%s Model:%d" % (category, i)
            item.thumbnail = self.thumbnail[category]
            item.description = self.description[category]
            item.price = random.randint(20, 99) + random.random()
            item.rating = random.randint(1, 10)
            item.save()
        self.stdout.write("128 items are created.")
