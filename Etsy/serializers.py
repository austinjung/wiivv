from rest_framework import serializers
from Etsy.models import Item
from Etsy.utils import get_thumbnail_resource_url


class ItemSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.HyperlinkedIdentityField(view_name='items-detail')
    description = serializers.SerializerMethodField()
    thumbnail = serializers.SerializerMethodField()
    usd_price = serializers.DecimalField(source='price', decimal_places=2, max_digits=10)
    cad_price = serializers.SerializerMethodField()
    gbp_price = serializers.SerializerMethodField()
    eur_price = serializers.SerializerMethodField()

    class Meta:
        model =Item
        fields = ('id', 'title', 'thumbnail', 'description', 'usd_price', 'cad_price', 'eur_price', 'gbp_price', 'rating')

    def get_cad_price(self, item):
        return "%.2f" % (getattr(self._context['view'], 'currency_table', None)['USDCAD'] * float(item.price))

    def get_gbp_price(self, item):
        return "%.2f" % (getattr(self._context['view'], 'currency_table', None)['USDGBP'] * float(item.price))

    def get_eur_price(self, item):
        return "%.2f" % (getattr(self._context['view'], 'currency_table', None)['USDEUR'] * float(item.price))

    def get_description(self, item):
        return item.description.split('\n')[:-1]

    def get_thumbnail(self, item):
        return get_thumbnail_resource_url(self._context['view'].request._request.META, item.thumbnail)
