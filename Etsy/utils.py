from django.conf import settings
from django.core.urlresolvers import RegexURLPattern, RegexURLResolver, LocaleRegexURLResolver
from django.core.exceptions import ViewDoesNotExist
from django.utils import translation


class ShowUrls(object):

    def get_endpoints(self, request):
        endpoints = []
        try:
            urlconf = __import__(getattr(settings, 'ROOT_URLCONF'), {}, {}, [''])
            view_functions = self.extract_views_from_urlpatterns(urlconf.urlpatterns)
            protocol = request.META['SERVER_PROTOCOL'].split('/')[0].lower()
            host = request.META['HTTP_HOST']
            for (func, regex, url_name) in view_functions:
                if regex.startswith('^api/'):
                    if 'pk' in regex:
                        url = regex.replace('^api/', '%s/api/' % (host)).replace('$', '')\
                                   .replace('(?P<pk>[^/.]+)', '<id>').replace('(?P<pk>[\\w\\.]+)', '<id>')\
                                   .replace('(?P<pk>[^/?]+)', '<id>')
                    else:
                        url = regex.replace('^api/', '%s://%s/api/' % (protocol, host)).replace('$', '')\
                                   .replace('(?P<pk>[^/.]+)', '<id>').replace('(?P<pk>[\\w\\.]+)', '<id>')\
                                   .replace('^', '')
                    if url_name:
                        endpoints.append('Name: %s, Url: %s' % (url_name, url))
        except Exception as e:
            pass
        return endpoints

    def extract_views_from_urlpatterns(self, urlpatterns, base='', namespace=None):
        """
        Return a list of views from a list of urlpatterns.

        Each object in the returned list is a two-tuple: (view_func, regex)
        """
        views = []
        for p in urlpatterns:
            if isinstance(p, RegexURLPattern):
                try:
                    if not p.name:
                        name = p.name
                    elif namespace:
                        name = '{0}:{1}'.format(namespace, p.name)
                    else:
                        name = p.name
                    views.append((p.callback, base + p.regex.pattern, name))
                except ViewDoesNotExist:
                    continue
            elif isinstance(p, RegexURLResolver):
                try:
                    patterns = p.url_patterns
                except ImportError:
                    continue
                if namespace and p.namespace:
                    _namespace = '{0}:{1}'.format(namespace, p.namespace)
                else:
                    _namespace = (p.namespace or namespace)
                if isinstance(p, LocaleRegexURLResolver):
                    for langauge in self.LANGUAGES:
                        with translation.override(langauge[0]):
                            views.extend(self.extract_views_from_urlpatterns(patterns, base + p.regex.pattern, namespace=_namespace))
                else:
                    views.extend(self.extract_views_from_urlpatterns(patterns, base + p.regex.pattern, namespace=_namespace))
            elif hasattr(p, '_get_callback'):
                try:
                    views.append((p._get_callback(), base + p.regex.pattern, p.name))
                except ViewDoesNotExist:
                    continue
            elif hasattr(p, 'url_patterns') or hasattr(p, '_get_url_patterns'):
                try:
                    patterns = p.url_patterns
                except ImportError:
                    continue
                views.extend(self.extract_views_from_urlpatterns(patterns, base + p.regex.pattern, namespace=namespace))
            else:
                raise TypeError("%s does not appear to be a urlpattern object" % p)
        return views


def get_thumbnail_resource_url(request_meta, resource):
    if resource in [None, '']:
        return None
    else:
        return request_meta['SERVER_PROTOCOL'].split('/')[0].lower() + '://' + \
               request_meta['HTTP_HOST'] + resource
