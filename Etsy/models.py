from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class Item(models.Model):
    title = models.CharField(max_length=128)
    thumbnail = models.CharField(max_length=256)
    description = models.TextField(max_length=1024)
    price = models.DecimalField(max_digits=10, decimal_places=2)  # price unit is USD based
    rating = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(5)], default=5)

    class Meta:
        app_label = 'Etsy'
        db_table = 'items'

    def __unicode__(self):
        return self.title
