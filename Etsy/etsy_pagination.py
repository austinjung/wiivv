from rest_framework.pagination import PageNumberPagination


class EtsyPagination(PageNumberPagination):

    def get_paginated_response(self, data):
        response = super(EtsyPagination, self).get_paginated_response(data)
        response.data['page_size'] = self.get_page_size(self.request)
        return response
