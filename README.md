# README #

This a repository for Wiivv assessment.

### What is this repository for? ###

* Quick summary
Etsy is an online eCommerce store for handmade craft products. We want to build a simple app which gets top products from Etsy API and displays them in a list. The list should display the title of the item (any image thumbnail which is provided by the API). The list should be able to page and display 10 products on a single page of the product listing.
* Currency
We also want to provide functionality to the user to view the list of products and their pricing in any currency they want. We want to support the following currencies:
 
1. GBP
2. CAD
3. USD
4. EUR
 
You can get the latest exchange rates from a public API like https://currencylayer.com/

## Setup application
- [Install Python/Django and Verify](https://docs.djangoproject.com/en/1.10/intro/install/)
- [Install pip or upgrade pip](https://pip.pypa.io/en/stable/installing/)
- [Install virtualenv](https://virtualenv.pypa.io/en/stable/installation/)
- change directory to the project top folder which has manage.py and requirements.txt
- run 'pip install -r requirements.txt' to install all required packages for this application
- run 'python manage.py collectstatic' to collect all static files for this application
- run 'python manage.py migrate' to migrate models schema 
- run 'python manage.py create_items' to create items in DB 
- run 'python manage.py runserver 0.0.0.0:8000' to run this application
- open web browser and visit 'http://localhost:8000'

## Live site
- [Austin's cloud server](http://wiivv.somoon.info/)
- [API](http://wiivv.somoon.info/api/)

### Algorithm challenge ###

* Get black rectangle
Imagine we have an image where every pixel is white or black.  We’ll represent this image as a simple 2D array (0 = black, 1 = white).  The image you get is known to have a single black rectangle on a white background.  Your goal is to find this rectangle and return its coordinates.
```
var image = [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1],
    [1, 0, 1, 0, 0, 1, 1],
    [1, 1, 1, 0, 0, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
];
```
* Run get_black_rectangle.py
- change directory to the project top folder which has manage.py and get_black_rectangle.py
- run 'python get_black_rectangle.py'.
- You can test with modify 
```
def get_black_rectangle(two_dim_arr):
    """
    :param two_dim_arr:
    :return: list of black rectangle left-top and right-bottom coordinator with zero-based index
    """

    black_rectangle_list = []  # return value: list of black rectangle left-top and right-bottom coordinator with zero-based index

    rows = len(two_dim_arr)  # rows = 3
    cols = len(two_dim_arr[0])  # cols = 4

    if rows == 0 or cols == 0:  # empty two dimensional array, return empty list of black rectangle
        return black_rectangle_list

    visited = [[False for c in range(cols)] for row in range(rows)]  # For memorize visited cell
    for row in range(rows):  # from top row
        for col in range(cols):  # from left column
            if two_dim_arr[row][col] == 0 and visited[row][col] is False:  # if current cell is start of a new black rectangle
                left_top = (row, col)
                right_bottom = left_top  # right_bottom initial value is same as left_top
                right_bottom = mark_field(two_dim_arr, row, col, rows, cols, visited, right_bottom)
                black_rectangle_list.append((left_top, right_bottom))

    return black_rectangle_list


def mark_field(arr, row, col, rows, cols, visited, right_bottom):
    if visited[row][col]:
        return right_bottom
    visited[row][col] = True
    if arr[row][col] == 1:
        return right_bottom
    if row > right_bottom[0]:
        right_bottom = (row, right_bottom[1])
    if col > right_bottom[1]:
        right_bottom = (right_bottom[0], col)
    ## This is a stating point:upper left of new field
    ## Start marking same field as visited
    if row > 0 and arr[row-1][col] == 0:
        right_bottom = mark_field(arr, row-1, col, rows, cols, visited, right_bottom)
    if row + 1 < rows and arr[row+1][col] == 0:
        right_bottom = mark_field(arr, row+1, col, rows, cols, visited, right_bottom)
    if col > 0 and arr[row][col-1] == 0:
        right_bottom = mark_field(arr, row, col-1, rows, cols, visited, right_bottom)
    if col + 1 < cols and arr[row][col+1] == 0:
        right_bottom = mark_field(arr, row, col+1, rows, cols, visited, right_bottom)
    return right_bottom


if __name__ == '__main__':
    two_dimension =  [
        [1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 1],
        [1, 0, 1, 0, 0, 0, 1],
        [1, 0, 1, 1, 1, 1, 1],
        [1, 0, 1, 0, 0, 1, 1],
        [1, 1, 1, 0, 0, 1, 1],
        [1, 1, 1, 1, 1, 1, 1],
    ]
    print get_black_rectangle(two_dimension)
```
